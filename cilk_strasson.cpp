#include <iostream>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

int** a11;
int** a12;
int** a21;
int** a22;
int** b11;
int** b12;
int** b21;
int** b22;
int** m1;
int** m2;
int** m3;
int** m4;
int** m5;
int** m6;
int** m7;
int** c11;
int** c12;
int** c21;
int** c22;

int** result;

void m1_func(int n);
void m2_func(int n);
void m3_func(int n);
void m4_func(int n);
void m5_func(int n);
void m6_func(int n);
void m7_func(int n);

struct timeval tvBegin;
struct timeval tvEnd;
struct timeval tvDiff;

int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1)
{
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

    return (diff<0);
}

void timeval_print(struct timeval *tv)
{
    char buffer[30];
    time_t curtime;

    printf("%ld.%06ld", tv->tv_sec, tv->tv_usec);
    curtime = tv->tv_sec;
    strftime(buffer, 30, "%m-%d-%Y  %T", localtime(&curtime));
    printf(" = %s.%06ld\n", buffer, tv->tv_usec);
}

void matrix_multiply(int n, int** matrix1, int** matrix2, int** result){
    int i,j,k;
    int sum = 0;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                sum = sum + matrix1[i][k]*matrix2[k][j];
            }
            result[i][j] = sum;
            sum = 0;
        }
    }
}

void matrix_add(int n, int** matrix1, int** matrix2, int** result)
{
    //int i,j;
    cilk_for (int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            result[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }
}

void matrix_sub(int n, int** matrix1, int** matrix2, int** result)
{
    cilk_for (int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
            result[i][j] = matrix1[i][j] - matrix2[i][j];
        }
    }
}

int clean_stdin()
{
    while (getchar()!='\n');
    return 1;
}


void matrix_print(int** matrix, int n)
{
    int i,j;
    printf("\nmatrix:\n");
    for(i = 0; i < n; i++) {
        for (j = 0; j < n; j++)
            printf("%d ", matrix[i][j]);
        printf("\n");
    }
    return;
}

int** matrix_alloc(int** matrix, int n)
{
    int i,j;
    matrix = (int**)malloc(n * sizeof(int *));
    for(i = 0; i < n; i++)
        matrix[i] = (int*)malloc(n * sizeof(int));
    return matrix;
}

int** matrix_calloc(int** matrix, int n)
{
    int i,j;
    matrix = (int**)calloc((size_t)n , sizeof(int *));
    for(i = 0; i < n; i++)
        matrix[i] = (int*)calloc((size_t) n , sizeof(int));
    return matrix;
}

int** matrix_init(int** matrix, int n)
{
    int i,j;
    matrix = (int**)malloc(n * sizeof(int *));
    for(i = 0; i < n; i++)
        matrix[i] = (int*)malloc(n * sizeof(int));
    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
            matrix[i][j] = (rand() % 100) + 1;
    return matrix;
}


int main() {
    int i,j,n, t;
    n = 2048;
    char c;
    string t_string;
    srand((unsigned int) time(NULL));
    // getting dimensions of matrices
//    do
//    {
//        printf("\nEnter dimensions of matrices: ");
//
//    } while ((scanf("%d%c", &n, &c)!=2 || c!='\n') && clean_stdin());
    // getting number of threads
//    do
//    {
//        printf("\nEnter number of workers: ");
//
//    } while ((scanf("%d%c", &t, &c)!=2 || c!='\n') && clean_stdin());
//

    // MATRIX INIT


    a11 = matrix_init(a11, n/2);
    a12 = matrix_init(a12, n/2);
    a21 = matrix_init(a21, n/2);
    a22 = matrix_init(a22, n/2);
    b11 = matrix_init(b11, n/2);
    b12 = matrix_init(b12, n/2);
    b21 = matrix_init(b21, n/2);
    b22 = matrix_init(b22, n/2);


    // MATRIX C allocation

    c11 = matrix_calloc(c11,n/2);
    c12 = matrix_calloc(c12,n/2);
    c21 = matrix_calloc(c21,n/2);
    c22 = matrix_calloc(c22,n/2);


    // RESULT MALLOC

    result = matrix_alloc(result, n);

//    printf("printing in this order: a11, a12, a21, a22, b11, b12, b21, b22\n");
//    matrix_print(a11,n/2);
//    matrix_print(a12,n/2);
//    matrix_print(a21,n/2);
//    matrix_print(a22,n/2);
//    matrix_print(b11,n/2);
//    matrix_print(b12,n/2);
//    matrix_print(b21,n/2);
//    matrix_print(b22,n/2);
    gettimeofday(&tvBegin, NULL);

    cilk_spawn m1_func(n/2);
    cilk_spawn m2_func(n/2);
    cilk_spawn m3_func(n/2);
    cilk_spawn m4_func(n/2);
    cilk_spawn m5_func(n/2);
    cilk_spawn m6_func(n/2);
    cilk_spawn m7_func(n/2);
    cilk_sync;

//    m1_func(n/2);
//    m2_func(n/2);
//    m3_func(n/2);
//    m4_func(n/2);
//    m5_func(n/2);
//    m6_func(n/2);
//    m7_func(n/2);


    matrix_add(n/2, c11, m1, c11);
    matrix_add(n/2, c11, m4, c11);
    matrix_sub(n/2, c11, m5, c11);
    matrix_add(n/2, c11, m7, c11);

    matrix_add(n/2, c12, m3, c12);
    matrix_add(n/2, c12, m5, c12);

    matrix_add(n/2, c21, m2, c21);
    matrix_add(n/2, c21, m4, c21);

    matrix_add(n/2, c22, m1, c22);
    matrix_sub(n/2, c22, m2, c22);
    matrix_add(n/2, c22, m3, c22);
    matrix_add(n/2, c22, m6, c22);


    for(i=0;i<n/2;i++)
    {
        for(j=0;j<n/2;j++)
        {
            result[i][j] = c11[i][j];
            result[i][j+n/2] = c12[i][j];
            result[i+n/2][j] = c21[i][j];
            result[i+n/2][j+n/2] = c22[i][j];
        }
    }
    gettimeofday(&tvEnd, NULL);
    timeval_subtract(&tvDiff, &tvEnd, &tvBegin);
    printf("%ld.%06ld\n", tvDiff.tv_sec, tvDiff.tv_usec);

    //   print result matrix
//    printf("printing result:\n");
//    matrix_print(result,n);

    for(i = 0; i < n/2; i++) {
        free(m1[i]);
        free(m2[i]);
        free(m3[i]);
        free(m4[i]);
        free(m5[i]);
        free(m6[i]);
        free(m7[i]);
        free(c11[i]);
        free(c12[i]);
        free(c21[i]);
        free(c22[i]);
        free(a11[i]);
        free(a21[i]);
        free(a12[i]);
        free(a22[i]);
        free(b11[i]);
        free(b12[i]);
        free(b21[i]);
        free(b22[i]);
        free(result[i]);
    }
    free(m1);
    free(m2);
    free(m3);
    free(m4);
    free(m5);
    free(m6);
    free(m7);
    free(c11);
    free(c12);
    free(c21);
    free(c22);
    free(a11);
    free(a21);
    free(a12);
    free(a22);
    free(b11);
    free(b12);
    free(b21);
    free(b22);
    free(result);
    return 0;
}

void m1_func(int n)
{
    int** temp1;
    int** temp2;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));

    temp2 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp2[i] = (int*)malloc((n) * sizeof(int));

    m1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m1[i] = (int*)malloc((n) * sizeof(int));

    matrix_add(n,a11,a22,temp1);
    matrix_add(n,b11,b22,temp2);
    matrix_multiply(n,temp1, temp2,m1);


    for(i = 0; i < n; i++) {
        free(temp1[i]);
        free(temp2[i]);
    }
    free(temp1);
    free(temp2);
    //printf("m1 Done!\n");
    return;
}


void m2_func(int n)
{
    int** temp1;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));
    m2 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m2[i] = (int*)malloc((n) * sizeof(int));

    matrix_add(n,a21,a22,temp1);
    matrix_multiply(n,temp1, b11,m2);


    for(i = 0; i < n; i++) {
        free(temp1[i]);
    }
    free(temp1);
    //printf("m2 Done!\n");
    return;
}
void m3_func(int n)
{
    int** temp1;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));
    m3 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m3[i] = (int*)malloc((n) * sizeof(int));

    matrix_sub(n,b12,b22,temp1);
    matrix_multiply(n,a11, temp1,m3);

    for(i = 0; i < n; i++) {
        free(temp1[i]);
    }
    free(temp1);
    //printf("m3 Done!\n");
    return;
}


void m4_func(int n)
{
    int** temp1;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));
    m4 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m4[i] = (int*)malloc((n) * sizeof(int));

    matrix_sub(n,b21,b11,temp1);
    matrix_multiply(n,a22, temp1,m4);


    for(i = 0; i < n; i++) {
        free(temp1[i]);
    }
    free(temp1);
    //printf("m4 Done!\n");
    return;
}

void m5_func(int n)
{
    int** temp1;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));
    m5 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m5[i] = (int*)malloc((n) * sizeof(int));

    matrix_add(n,a11,a12,temp1);
    matrix_multiply(n,temp1, b22,m5);

    for(i = 0; i < n; i++) {
        free(temp1[i]);
    }
    free(temp1);
    //printf("m5 Done!\n");
    return;
}

void m6_func(int n)
{
    int** temp1;
    int** temp2;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));

    temp2 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp2[i] = (int*)malloc((n) * sizeof(int));

    m6 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m6[i] = (int*)malloc((n) * sizeof(int));

    matrix_sub(n,a21,a11,temp1);
    matrix_add(n,b11,b12,temp2);
    matrix_multiply(n,temp1, temp2,m6);


    for(i = 0; i < n; i++) {
        free(temp1[i]);
        free(temp2[i]);
    }
    free(temp1);
    free(temp2);
    //printf("m6 Done!\n");
    return;


}


void m7_func(int n)
{
    int** temp1;
    int** temp2;
    int i;
    temp1 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp1[i] = (int*)malloc((n) * sizeof(int));

    temp2 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        temp2[i] = (int*)malloc((n) * sizeof(int));

    m7 = (int**)malloc((n) * sizeof(int *));
    for(i = 0; i < n; i++)
        m7[i] = (int*)malloc((n) * sizeof(int));

    matrix_sub(n,a12,a22,temp1);
    matrix_add(n,b21,b22,temp2);
    matrix_multiply(n,temp1, temp2,m7);


    for(i = 0; i < n; i++) {
        free(temp1[i]);
        free(temp2[i]);
    }
    free(temp1);
    free(temp2);
    //printf("m7 Done!\n");
    return;

}
